FROM busybox

WORKDIR /app

ADD https://dl.bintray.com/mitchellh/serf/0.6.4_linux_amd64.zip /app/0.6.4_linux_amd64.zip
RUN unzip 0.6.4_linux_amd64.zip
	&& rm 0.6.4_linux_amd64.zip

ADD . /app/

EXPOSE 7946 7373

CMD ["./start"]